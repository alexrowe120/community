## SIG组名称
UKUIDesign
本sig组负责操作系统（主题控件&SDK、壁纸、音效、图标、光标、界面布局等）设计规范制定、设计、维护等工作，致力于提供操作系统各主题规范。

## 工作目标
提供openkylin社区的设计规范，与广大社区爱好者分享和学习交流。

## SIG成员
liulihua@kylinos.cn

ouyangyu@kylinos.cn

yangmingxing@kylinos.cn

## 邮件列表
ukuidesign@lists.openkylin.top
