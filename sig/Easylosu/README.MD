## Easylosu-SIG组

## 工作目标
Easylosu SIG组（Easylosu Special Interest Group，简称Easylosu SIG）
随着开放麒麟操作系统的普及，普通用户的编程需求会逐渐凸显，现有linux生态的sh，python脚本等编程语言对于无基础的普通用户并不友好，我们EasyLosu SIG组负责为开放麒麟开发简单高效的编程语言，致力于让用户以最低的门槛，轻松享受编程的便利，促进编程语言国产化，促进开放麒麟在非开发者群体的推广。

## SIG成员

+ owner
    - 陈朝臣(3174025065@qq.com)  Gitee ID chen-chaochen
## SIG维护包列表
- easylosu

## 邮件列表
easylosu@lists.openkylin.top