# 实时性增强SIG（RTE SIG）

实时性增强小组（RTE SIG）致力于实时性增强方面的研究，通过内核实时性扩展、内核轻量化等技术，提升openKylin开源操作系统项目的实时性。

## 工作目标

1. 提高openKylin操作系统在实时性方面的可维护性和可扩展性，以满足不断变化的实时性需求和挑战。
2. 通过实时性增强提升用户对openKylin的信心，同时促进社区内的知识共享。

## SIG成员

### Owner
- fukai(fukai@ecict.com.cn)

### Maintainers
- 刘颖娜(openkylin02@ecict.com.cn)
- 姚远(openkylin01@ecict.com.cn)

## SIG 维护包列表

(建设中)